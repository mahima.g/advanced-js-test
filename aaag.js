const utilities = require('./utilities.js');
const friendUtilities = require('./friendsUtilities.js');
const loadDashboard = function loadDashboard(){

console.log('Welcome to the social application.');
console.log('Please select an option from below to continue.');
console.log('1. View all users');
console.log('2. posts made by your friends');
console.log('3. View friends');
console.log('4. View friend');
console.log('5. View posts');
console.log('6. Add friend');
console.log('7. Add post');
console.log('8. Delete post');
console.log('9. Delete friend');
console.log('10. Logout');

var choice = process.stdin;
choice.setEncoding('utf-8');
console.log('Enter your choice:');
choice.on('data', function (data) {

    // User input exit.
    if (data === '10') {
        // Program exit.
        console.log("User input complete, program exit.");
        process.exit();
    } else {
        // Print user input in console.
        console.log('User Input Data : ' + data);
        switch (data) {
            case '1\n':
                console.log('====1');
                friendUtilities.viewUsers();
                break;
            case '2\n':
                friendUtilities.viewFriendsPost();
                break;
            case '3\n':
                friendUtilities.viewFriends();
                break;
            case '4\n':
                friendUtilities.viewFriend();
                break;
            case '5\n':
                utilities.viewPosts();
                break;
            case '6\n':
                friendUtilities.addFriend();
                break;
            case '7\n':
                utilities.addPost();
                break;
            case '8\n':
                utilities.deletePost();
                break;
            case '9\n':
                friendUtilities.deleteFriend();
                break;
            case '10\n':
                process.exit();
                break;
            default:
                

        }
    }
});
}


module.exports = {
    loadDashboard : loadDashboard
}