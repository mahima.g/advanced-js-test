'use strict'
const fs = require('fs');
const auth = require('./AuthService.js');
const utilities = require('./utilities.js');
const dashboard = require('./aaag.js');

console.log('Enter your credentials below to login');
const readline = require('readline')

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
})

const question1 = () => {
    return new Promise((resolve, reject) => {
        rl.question('Enter your email ID: ', (answer) => {
            console.log(`Your name: ${answer}`)
            resolve(`${answer}`)
        })
    })
}

const question2 = () => {
    return new Promise((resolve, reject) => {
        rl.question('Enter your password: ', (answer) => {
            console.log(`Your email: ${answer}`)
            resolve(`${answer}`)
        })
    })
}


var email = '';
var password = '';




const main = async () => {
    email = await question1()
    password = await question2()
    const result = auth.signin(email, password);
    if(result){
        dashboard.loadDashboard();
        module.exports.email = email
    }
    else{
        console.log('Invalid username and password');
    }
   
  
}

main()



