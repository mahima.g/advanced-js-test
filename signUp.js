'use strict'
const fs = require('fs');
var SHA512 = require('crypto-js/sha512');



const readline = require('readline')

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

const question1 = () => {
  return new Promise((resolve, reject) => {
    rl.question('Enter user full name ', (answer) => {
      console.log(`Your name: ${answer}`)
      resolve(`${answer}`)
    })
  })
}

const question2 = () => {
  return new Promise((resolve, reject) => {
    rl.question('enter user email ', (answer) => {
      console.log(`Your email: ${answer}`)
      resolve(`${answer}`)
    })
  })
}

const question3 = () => {
    return new Promise((resolve, reject) => {
      rl.question('enter user password ', (answer) => {
        //console.log(`Your : ${answer}`)
        resolve(`${answer}`)
      })
    })
  }

var name = '';
var email = '';
var password = '';




const addUser = (name_, email_, password_) => {
    var min=1;
    var max=100000;
    var random = Math.floor(Math.random() * (+max - min) + +min);
    
    console.log(name_, ' ', email_, ' ', password_ );
   const user = loadUsers();
   user.push({
       id: random,
       name: name,
       email: email,
       password: SHA512(password).toString(),
       friends: []
   });
   saveUser(user);
}

const saveUser = function(user){
    const dataJSON = JSON.stringify(user);
    fs.writeFileSync('userDetails.json',dataJSON);
}

const loadUsers = function loadUsers(){
    try{
        const dataBuffer = fs.readFileSync('userDetails.json');
        const dataJSON = dataBuffer.toString();
        return JSON.parse(dataJSON);
    } catch(e){
        return [];
    }
}

const main = async () => {
    name = await question1()
    email = await question2()
   password = await question3()
   // console.log(name, ' ', email, ' ', password );
    addUser(name,email,password);
    rl.close()
  }
  
  main()
 