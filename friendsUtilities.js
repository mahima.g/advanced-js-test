
const fs = require('fs');
const login = require('./login.js');
const friendService = require('./FriendService.js');
const postService = require('./postService.js');
const readline = require('readline')


var id = 0;
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
})

const question1 = () => {
    return new Promise((resolve, reject) => {
        rl.question('Enter user id to add friend:-  ', (answer) => {
        //    console.log(`Your name: ${answer}`)
            resolve(`${answer}`)
        })
    })
}

const viewfriendID = () => {
    return new Promise((resolve, reject) => {
        rl.question('Enter friend ID:-  ', (answer) => {
        //    console.log(`Your name: ${answer}`)
            resolve(`${answer}`)
        })
    })
}

const deleteFriendId = () => {
    return new Promise((resolve, reject) => {
        rl.question('Enter Friend ID to delete ', (answer) => {
            // console.log(`Your name: ${answer}`)
            resolve(`${answer}`)
        })
    })
}

const loadFriends = function loadFriends(){
    try{
        const dataBuffer = fs.readFileSync('userDetails.json');
        const dataJSON = dataBuffer.toString();
        return JSON.parse(dataJSON);
    } catch(e){
        return [];
    }
   
}

const loadSelf = function loadSelf(){
    try{
        const dataBuffer = fs.readFileSync('userDetails.json');
        const dataJSON = dataBuffer.toString();
        return JSON.parse(dataJSON);
    } catch(e){
        return [];
    }
}

const addFriend = async function(){
    const post = await question1();
    friendService.addFriend(post);
    // const user = loadFriends();
    // const posts = loadSelf();
    // posts.forEach(element => {
    //     if(element.email === login.email){
    //       // console.log("UserID : " + element.id + "\t Name :" + element.name + 'Friends' + element.friends);
    //         element.friends.push(post);
    //     }      
    // });
    // const dataJSON = JSON.stringify(posts);
    // fs.writeFileSync('userDetails.json',dataJSON);

   
    
}





const viewUsers = function(){
    const posts = loadFriends();
    console.log('==Users==');
    posts.forEach(element => {
        if(element.email !== login.email){
            console.log("UserID : " + element.id + "\t Name :" + element.name);
        }      
    });
}

const viewFriends = function(){
    friendService.viewFriends(login.email);
   

}

const viewFriend = async function(){
    const viewFriend = await viewfriendID();
    friendService.viewFriend(viewFriend);
    // const posts = loadFriends();
    // console.log('YOUR FRIENDS');
    // posts.forEach(element => {
    //     if(element.id.toString() === viewFriend.toString() && element.email.toString() !== login.email.toString()){
    //      console.log('Friend ID :'+element.id + '\tName :' +element.name);  
    //     }      
    // });
}


const deleteFriend = async function(){
    const deleteFriend1 = await deleteFriendId();
    friendService.removeFriend(deleteFriend1);
    // const posts = loadFriends();
    // const postsToKeep = posts.forEach((element) => {
    //     for(var i = 0; i<element.friends.length;i++){
    //         if(element.friends[i] === deleteFriend1){
    //          return delete element.friends[i];
    //            return element.friends.splice(i,1);
    //         }
    //     }
    // })
  
    // saveFriend(posts); 


}

const viewPosts = function(){
    const posts = loadPosts();
    console.log('==YOUR POSTS==');
    posts.forEach(element => {
        if(element.email === login.email){
            console.log("PostID : " + element.id + "\tText :" + element.text);
        }   
       
    });
}

const viewFriendsPost = async function(){
    postService.displayGetPosts();
    //  const posts = loadPosts();
    //  console.log('==FRIEND\'s POSTS==');
    
    // var arr = [];
    // var email =[];
    // const friends = loadFriends();
    // friends.forEach(element => {
    //     if(element.email === login.email ){
    //         arr = element.friends;
    //     }      
    // });
    // arr.forEach((element) => {
    //     friends.forEach(val => {
    //         if(element.toString() === val.id.toString() ){
    //             email.push(val.email.toString());
    //         }      
    //     });
    // });
    // email.forEach((element) => {
    //     posts.forEach(val => {
    //         if(element.toString() === val.email.toString() ){
    //             console.log('Post: ',val.text.toString());
    //         }      
    //     });
    // });

    
}

const saveFriend = function(Friend){
    const dataJSON = JSON.stringify(Friend);
    fs.writeFileSync('userDetails.json',dataJSON);
}

const loadPosts = function loadPosts(){
    try{
        const dataBuffer = fs.readFileSync('posts.json');
        const dataJSON = dataBuffer.toString();
        return JSON.parse(dataJSON);
    } catch(e){
        return [];
    }
}

module.exports = {
    viewUsers : viewUsers,
    addFriend : addFriend,
    viewFriends : viewFriends,
    viewFriend : viewFriend,
    deleteFriend : deleteFriend,
    viewFriendsPost : viewFriendsPost,
    loadFriends : loadFriends,
    loadSelf : loadSelf
}
