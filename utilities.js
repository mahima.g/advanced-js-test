const fs = require('fs');
const login = require('./login.js')
const postServices = require('./postService.js')
const readline = require('readline')
var id = 0;
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
})
const question1 = () => {
    return new Promise((resolve, reject) => {
        rl.question('post text(enter here): ', (answer) => {
            // console.log(`Your name: ${answer}`)
            resolve(`${answer}`)
        })
    })
}

const deletePostId = () => {
    return new Promise((resolve, reject) => {
        rl.question('Enter post ID ', (answer) => {
            // console.log(`Your name: ${answer}`)
            resolve(`${answer}`)
        })
    })
}
const viewAllUser = function(){
    console.log('All users');
}

const viewFriendsPost = function(){
    console.log(object);
}

const viewFriends = function(){
    console.log(object);
}

const viewFriend = function(){
    console.log(object);
}

const addFriend = function(){
    console.log(object);
}

const viewPosts = function(){
    const posts = loadPosts();
    console.log('==YOUR POSTS==');
    posts.forEach(element => {
        if(element.email === login.email){
            console.log("PostID : " + element.id + "\tText :" + element.text);
        }      
    });
}


const addPost = async function(){
    // var min=1;
    // var max=100000;
    // var random = Math.floor(Math.random() * (+max - min) + +min);

    console.log('Add a new post');
    const post = await question1();
    postServices.addPost(post);
    // const user = loadPosts();
//     user.push({
//         id : random,
//         email : login.email,
//         text : post,
//         likes : 0
//     });
//    savePost(user);

  
    
}

const deletePost = async function(){
    const deletePostId1 = await deletePostId();
    postServices.deletePost(deletePostId1);
    // const posts = loadPosts();
    // //console.log(posts);
    // const postsToKeep = posts.filter((posts)=> {
    //     return posts.id.toString() !== deletePostId1.toString();
    // });

    

    // if(posts.length > postsToKeep.length){
    //     console.log('Post is removed');
    //     savePost(postsToKeep);
    // }
    // else{
    //     console.log('No post found');
    // }

    // savePost(postsToKeep); 


}



const loadPosts = function loadPosts(){
    try{
        const dataBuffer = fs.readFileSync('posts.json');
        const dataJSON = dataBuffer.toString();
        return JSON.parse(dataJSON);
    } catch(e){
        return [];
    }
}


const savePost = function(Post){
    const dataJSON = JSON.stringify(Post);
    fs.writeFileSync('posts.json',dataJSON);
}

module.exports = {
    viewAllUser : viewAllUser,
    addPost : addPost,
    deletePost : deletePost,
    viewPosts : viewPosts
}

