/**
 * Class represents services for users.
 */
const fs = require('fs');
const utilities = require('./utilities.js');
var SHA512 = require('crypto-js/sha512');
// const dashboard = require('./aaag.js');

class AuthService {
    /**
     * This function use to login/signin user into system.
     * @param {string}  email  email required details 
     * @param {string}  password  password required details 
     * @returns {Object} success or false object
     */
    static async signin(email,password){
        try{
            const dataBuffer = fs.readFileSync('userDetails.json');
            const dataJSON = dataBuffer.toString();
            const parsedData = JSON.parse(dataJSON);
            password = SHA512(password);

            const user = parsedData.find((user) => {
                return email=== user.email && password.toString() === user.password.toString();
            });

            if(user){
               // console.log('PRESENT');
                //dashboard.loadDashboard();
                return {
                    status: {
                        success: true,
                        message: 'success'
                    }
                }

            }
            else{
                console.log('User doesn\'t exist');
                process.exit();
            }
        } catch(e){
            return [];
        }


        
    }

    /**
     * This function use to logout from system
     * @param {string}  email  email to logout user from system
     * @returns {Object} success or false object
     */
    static async logout(email){
        return {
            status: {
                success: true,
                message: 'success'
            }
        }
    }

   
}
    
module.exports = AuthService;