const fs = require('fs');
const login = require('./login.js');
class PostService {
    static addPost(post) {
        var min = 1;
        var max = 100000;
        var random = Math.floor(Math.random() * (+max - min) + +min);
        const user = this.loadPosts();
        user.push({
            id: random,
            email: login.email,
            text: post,
            likes: 0
        });
        this.savePost(user);
        // function accepts a post object, saves it in a file and returns the post object
        return {
            id: 1234,
            text: "dummy post",
            postBy: "User name",
            likes: 0
        }
    }
    static getposts() {
        const posts = this.loadPosts();
        console.log('==FRIEND\'s POSTS==');
       var result = [];
       var data = {};
       var arr = [];
       var email =[];
       const friends = this.loadFriends();
       friends.forEach(element => {
           if(element.email === login.email ){
               arr = element.friends;
           }      
       });
       arr.forEach((element) => {
           friends.forEach(val => {
               if(element.toString() === val.id.toString() ){
                   email.push({
                      email: val.email.toString(),
                    name: val.name.toString()
                });
               
               }      
           });
       });
       email.forEach((element) => {
           posts.forEach(val => {
               if(element.email.toString() === val.email.toString() ){
                //   console.log('Friend Name : ', element.name ,'Post: ',val.text.toString());
                data.name = element.name;
                data.text = val.text;
                result.push(data);
                data = {};
                }      
           });
       });
   
   
        // return's all posts made by the user's friends along with the friend's name and number of likes
        return result;
    }
    static getPost(id) {
        // function gets post details based on the ID
        return {
            id: 1234,
            text: "dummy post",
            postBy: "User name or friend's name",
            likes: 14
        }
    }
    static deletePost(id) {
        // deletes post based on ID
        const posts = this.loadPosts();
    //console.log(posts);
    const postsToKeep = posts.filter((posts)=> {
        return posts.id.toString() !== id.toString();
    });

    

    if(posts.length > postsToKeep.length){
        console.log('Post is removed');
        this.savePost(postsToKeep);
    }
    else{
        console.log('No post found');
    }

    this.savePost(postsToKeep); 
    }
    static savePost(Post) {
        const dataJSON = JSON.stringify(Post);
        fs.writeFileSync('posts.json', dataJSON);
    }
    static loadPosts(){
        try{
            const dataBuffer = fs.readFileSync('posts.json');
            const dataJSON = dataBuffer.toString();
            return JSON.parse(dataJSON);
        } catch(e){
            return [];
        }
    }
    static loadFriends() {
        try {
            const dataBuffer = fs.readFileSync('userDetails.json');
            const dataJSON = dataBuffer.toString();
            return JSON.parse(dataJSON);
        } catch (e) {
            return [];
        }

    }

    static displayGetPosts(){
        const result = this.getposts();
       // console.log(result);
        result.forEach(element => {
            console.log('Friend name: ',element.name,'\tPost--', element.text);
        })

    }

}


module.exports = PostService;
