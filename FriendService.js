/**
* Class represents services for friends.
*/
const fs = require('fs');
const login = require('./login.js');
const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
})
class FriendService {
    /**
     * This function use to add friend
     * @param {Object}  friend  friend required details to add friend
     * @returns {Object} success or false object
     */
    static deleteFriendId(){
        return new Promise((resolve, reject) => {
            rl.question('Enter Friend ID to delete ', (answer) => {
                // console.log(`Your name: ${answer}`)
                resolve(`${answer}`)
            })
        })
    }

    static viewfriendID() {
        return new Promise((resolve, reject) => {
            rl.question('Enter friend ID:-  ', (answer) => {
                //    console.log(`Your name: ${answer}`)
                resolve(`${answer}`)
            })
        })
    }

    static addFriend(friend) {
        const posts = this.loadSelf();
        posts.forEach(element => {
            if (element.email === login.email) {
                // console.log("UserID : " + element.id + "\t Name :" + element.name + 'Friends' + element.friends);
                element.friends.push(friend);
            }
        });
        const dataJSON = JSON.stringify(posts);
        fs.writeFileSync('userDetails.json', dataJSON);
        console.log('Friend is added.');
        return {
            status: {
                success: true,
                message: 'success'
            }
        }
    }
    /**
     * This function use to remove friend
     * @param {Integer}  friendId  friend primary id to remove friend from your friends list
     * @returns {Object} success or false object
     */
    static async removeFriend(friendId) {
        const posts = this.loadFriends();
        const postsToKeep = posts.forEach((element) => {
            for (var i = 0; i < element.friends.length; i++) {
                if (element.friends[i] === friendId) {
                    //  return delete element.friends[i];
                    return element.friends.splice(i, 1);
                }
            }
        })

        this.saveFriend(posts);

        return {
            status: {
                success: true,
                message: 'success'
            }
        }
    }
    /**
     * This function use to view friend details
     * @param {Integer}  friendId  friend primary id to view friend details
     * @returns {Object} friend details object
     */
    static viewFriend(friendId) {
        var id ='';
        var name ='';
        var email='';
        const posts = this.loadFriends();
        console.log('YOUR FRIENDS');
        posts.forEach(element => {
            if (element.id.toString() === friendId.toString() && element.email.toString() !== login.email.toString()) {
                console.log('Friend ID :' + element.id + '\tName :' + element.name);
                id=element.id;
                name=element.name;
                email=element.email;
            }
        });
        return {
            status: {
                success: true,
                message: 'success'
            },
            data: {
                id: id ,
                name: name,
                email: email
            }
        }
    }
    /**
     * This function use to view all friends list
     * @param {Integer}  id  Logged in user primary id to get all friends
     * @returns {Object} all friends object
     */
    static viewFriends(email) {
        var arr = [];
        const posts = this.loadFriends();
        console.log('==FRIENDS==');
        posts.forEach(element => {
            if(element.email === email ){
                arr = element.friends;
            }      
        });
        arr.forEach((element) => {
            posts.forEach(val => {
                if(element.toString() === val.id.toString() ){
                    console.log('Friend ID : ',val.id,'\tName :',val.name);
                }      
            });
        });
        return {
            status: {
                success: true,
                message: 'success'
            },
            data: []
        }
    }


    static loadFriends() {
        try {
            const dataBuffer = fs.readFileSync('userDetails.json');
            const dataJSON = dataBuffer.toString();
            return JSON.parse(dataJSON);
        } catch (e) {
            return [];
        }

    }

    static loadSelf() {
        try {
            const dataBuffer = fs.readFileSync('userDetails.json');
            const dataJSON = dataBuffer.toString();
            return JSON.parse(dataJSON);
        } catch (e) {
            return [];
        }
    }

    static saveFriend (Friend) {
        const dataJSON = JSON.stringify(Friend);
        fs.writeFileSync('userDetails.json', dataJSON);
    }

}
module.exports = FriendService;


